Minutes of the Solidary Administrative Council meeting of 2022-06-25


Present: Andreas Enge, Manolis Ragkousis, Julien Lepiller, Simon Tournier


1) Exclusion of members

It is decided unanimously to exclude two members who have not paid their
membership fees since 2020; the SAC stresses that they are welcome to
adhere again to Guix Europe in the future.

