Minutes of the Solidary Administrative Council meeting of 2023-06-13


Present: Andreas Enge, David Wilson, Efraim Flashner, Timo Wilken,
Jelle Licht, Adriel Dumas--Jondeau, Jonathan Brielmaier,
Christopher Baines, Tanguy Le Carrour


1) Membership requests

The membership requests by Arun Isaac and Graham Addis are accepted
unanimously.

