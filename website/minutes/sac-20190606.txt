Minutes of the Solidary Administrative Council meeting of 2019-06-06


Present: Ludovic Courtès, Andreas Enge, Ricardo Wurmus


1) Expenses

It is decided unanimously that Ricardo be reimbursed 33.32€ for two
years of domain registration for the guix.info domain (until 2020-06-21).

