title: GNU Guix Days FOSDEM 2019
date: 2022-11-13 0:00
---
This page was for tracking anything related with the 2019 GNU Guix/Guile
conference and hackathon on Jan. 31st and Feb. 1st — right before
[FOSDEM 2019](https://fosdem.org/2019/) and the [minimalistic language
devroom](https://libreplanet.org/wiki/FOSDEM2019-devroom-minimalism).

![Logo of the Guix Days](/images/Guix-Days-2019.png)

Annual GNU Guix (un)conference. This event is a [FOSDEM 2019 fringe
event](https://fosdem.org/2019/fringe/).

## Why?

GNU Guix is growing rapidly and has gone from a software packaging
system to a full tool stack aimed at reproducible software deployment
and development. GNU Guix is a toolkit that allows developers to
integrate reproducible software deployment into their applications — as
opposed to leaving it up to the user. GNU Guix is based on the GNU Guile
programming language which makes it a very versatile and hackable (in
the good sense) environment.

## When?

Thursday 31st of January and Friday 1st of February, 2019, the two days
before FOSDEM.

Coffee at 9:30AM, and starting at 10AM.

## Where?

[Institute of Cultural Affairs (ICAB)](http://icab.be/) Rue Amedee Lynen
8 1210 Brussels
([map](https://www.openstreetmap.org/?mlat=50.85019&mlon=4.37320#map=18/50.85019/4.37320)),
Belgium.

## Programme

We had some talks in the morning and hacking and discussions the
rest of the day. Two days of GNU Guix bliss.

The event targeted an audience of Guix developers and users.

In pure "unconference" style, the program was made by participants
as we went, with hands-on sessions organised in subgroups.
Session topics included:

1. GNU Guix road map
2. GNU Guix online documentation
3. Mes and bootstrapping
4. Reproducibility project
5. Alternative target architectures (ARM etc.)
6. Developing workflows (GNU Guix and GNU Workflow Language)
7. Support for D in GNU Guix
8. Demonstration and explanation of [The Perfect
    Setup](https://www.gnu.org/software/guix/manual/en/guix.html#The-Perfect-Setup)
    (Emacs, Geiser, Magit)
9. Demonstration and live-hacking of [Next
    browser](https://next.atlas.engineer) + introduction to the code
    base
10. Better build systems for JVM languages in GNU Guix (support maven,
    sbt, gradle, \...)
11. Reproducible JDK
12. The GuixSD installer
13. your topic?

## Code of conduct

Attendees implicitely abode by the [code of conduct as
stated by FOSDEM](https://fosdem.org/2019/practical/conduct/).

## Participants

1. Ludovic Courtès
2. Jonathan Brielmaier
3. Tobias (nckx) Geerinckx-Rice
4. Julien Lepiller
5. Chris Webber
6. Ricardo Wurmus
7. Pjotr Prins
8. Manolis Ragkousis
9. Alex Sassmannshausen
10. Andy Wingo
11. Christopher Baines
12. Gábor Boskovits
13. Clément Lassieur (Friday only)
14. Mathieu Othacehe (Friday only)
15. Pierre Neidhardt
16. Björn Höfling
17. Leo Famulari
18. Jan (janneke) Nieuwenhuizen
19. Efraim Flashner
20. Danny Milosavljevic
21. Mark Meyer
22. Tobias \"Tomoko\" Platen
23. Marco van Hulten — arriving afternoon of 2nd day
24. Laura Lazzati
25. Mike Gran
26. Chris Marusich
27. Jelle Licht
28. Andreas Enge
29. Simon Tournier
30. Danny Milosavljevic
31. Andy Patterson
32. Profpatsch
33. Christian Jaeger
34. Matias Jose
35. Adu O\'Hara

## Costs

Attendance was free. We asked for a voluntary contribution for
consumptions.

## Dinner

- Thursday: [Mont Liban](http://www.montliban.be/),
    [map](https://www.openstreetmap.org/?mlat=50.83020&mlon=4.35901#map=19/50.83020/4.35901)
- Friday:
    [Santorini](https://www.takeaway.com/be/santorini-bruxelles),
    [map](https://www.openstreetmap.org/?mlat=50.84577&mlon=4.35262#map=19/50.84577/4.35262)

## Sessions Outcome

### Maven Build System

- Maven uses plugins to implement build phases. We need to build at
    least a small set of plugins with our ant-build-system.
- One problem is that the plugin\'s jar file not only contains classes
    but also a meta-file describing the plugins interface.
- We can try to build this metafile with the
    maven-plugins-tools-generators, see
    [1](https://maven.apache.org/plugin-tools/).
- When we actually use the maven-build-system, it needs to generate a
    \".m2/repository\", artificially created from the package inputs.
- In order to make life with this step easier, we decided to modify
    the ant-build-system: It should already create its part of the
    system:
- I.e. when the pachage is called \"org.myproject:myartifact:1.0.3\",
    we should generate the path
    \"org/myproject/myartifact/1.0.3/myartifact-1.0.3.jar\" and symlink
    it to the actual jar-file the ant-build-system generated.
- We should modify the ant-build-system to copy/generate a pom.xml,
    either into the jar-file and/or besides it.

### Quality Assurance and Continuous Integration

- Chris Baines set up a patchwork instance:
- [2](https://patchwork.cbaines.net/)
- Currently it gets patches from the patches-mailinglist and does \...
    something with it.
- Plan: Next step would be to get some metrics out of the patchset: We
    want to measure if there is any change on linting errors (either new
    errors or errors fixed). Also it would be nice to measure the impact
    of the package: Either bei number of dependent packages or even by
    estimated compile-time of all dependent packages (by getting numbers
    from previous berlin compilations).
- For that, the idea is to apply the patchset on a Guix git checkout
    and do a full guix lint before and after, save it in a database and
    compare both states. For the numbers, we can execute a guix refresh
    -l.
- A next step would be to connect patchwork with a new Cuirass
    instance (independent of berlin server): Create for each patchset a
    new branch like \"PATCH-xxx\" where xxx is the debbugs bug number.
    The branch is given to Cuirass for evaluation.
- For that, we need a way to get that into Cuirass: Currently, the
    configuration is more static. We could either insert directly into
    the database or write an REST-interface (including authentication?)
    for Cuirass.
- Note: Cuirass can first count the number of rebuilds and we only
    evaluate the patchset if the number of rebuilds is below a certain
    threshold (say 1000) of rebuilds.
- We have a patchset for patchwork not yet ready for merging (#33185).
- Concerning packages not working for non-x86_64 distributions: We
    should work more on it, test them in the first place. If your
    package breaks on a system you are unfamiliar with, please ask for
    help on the mailing list.
- We have not discussed what we should do about packages that are
    long-term broken, but I would suggest to enforce the rules we
    learned in Frederic Crozat\'s talk from SuSE:
- If a package is broken for more than 6 months, we should just remove
    it from Guix.
- Prior to removing, we should announce on the dev mailing list, maybe
    someone will care about it then.

### Cuirass

- We have database issues: Some queries are very slow.
- Discussion: Move from SQLite to PostgreSQL?
- But is therea Guile-binding?
- There is \"gule-dbi\"?
- Or write a new binding?
- Or: Connect to PostgresSQL via TCP/Socket, use text-protocol to
    execute queries?
- We have to be careful: The Fibres-framework we use does not permit
    C-stackframes.
- Find some way to \"pin\" packages: For example, when we have a
    release, we want to add this build to gc-roots.
- Collecting build times locally?
- New guile-daemon written in Guile?

## Sponsors

- [GeneNetwork](http://gn2.genenetwork.org/)
- [JoyofSource](http://JoyofSource.com/)
- Foundation of Guix Europe

